(function() {
  var jessie, path, spawn;

  spawn = require("child_process").spawn;

  path = require("path");

  jessie = require("jessie");

  module.exports = function(grunt) {
    return grunt.registerMultiTask("jessie", "Runs Jasmine specs with Jessie", function() {
      var done, files;
      done = this.async();
      files = [];
      this.files.forEach(function(file) {
        var f, _i, _len, _ref, _results;
        _ref = file.src;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          f = _ref[_i];
          _results.push(files.push(f));
        }
        return _results;
      });
      grunt.verbose.writeflags(files, "Files");
      return jessie.run(files, {
        format: "progress"
      }, function(fail) {
        return done(!fail);
      });
    });
  };

}).call(this);
