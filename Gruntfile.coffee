module.exports = (grunt) ->
	grunt.initConfig
		pkg: "<json:package.json>"

		coffee:
			task:
				files:
					"tasks/jessie.js": "src/jessie.coffee"

		watch:
			files: ["src/**"]
			tasks: "default"

	grunt.loadNpmTasks "grunt-contrib-coffee"
	grunt.loadNpmTasks "grunt-contrib-watch"

	grunt.registerTask "default", ["coffee"]
