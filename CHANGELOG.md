# Changelog

## 0.0.3 (2013-07-31)
* *Fix:* Changed to using the jessie runner rather than the command line until.
* *Fix:* Errors occurring in the jessie runner itself would not fail grunt.

## 0.0.2 (2013-07-23)
* *Fix:* Jessie binary path resolution.

## 0.0.1 (2013-07-23)
* Initial version.
