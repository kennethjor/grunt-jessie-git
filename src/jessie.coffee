spawn = require("child_process").spawn
path = require "path"
jessie = require "jessie"

# Jessie task.
module.exports = (grunt) ->
	# Task defition.
	grunt.registerMultiTask "jessie", "Runs Jasmine specs with Jessie", ->
		done = @async()

		# Build file list.
		files = []
		@files.forEach (file) ->
			for f in file.src
				files.push f
		grunt.verbose.writeflags files, "Files"

		jessie.run files, format:"progress", (fail) ->
			done !fail
